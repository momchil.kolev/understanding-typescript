console.log("Testing things");
function outer() {
  console.log("outer this", this);
  return function() {
    console.log("inner this", this);
  };
}
outer.bind({})()();

function outer2() {
  console.log("outer this", this);
  return () => {
    console.log("inner this", this);
  };
}
outer2.bind({})()();
