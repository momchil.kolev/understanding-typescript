console.log("Testing things");
function outer() {
    console.log("outer this", this);
    return function () {
        console.log("inner this", this);
    };
}
outer.bind({})()();
function outer2() {
    var _this = this;
    console.log("outer this", this);
    return function () {
        console.log("inner this", _this);
    };
}
outer2.bind({})()();
